<?php

require_once __DIR__ . '/ApiHandler.php';
use DigitalResponse\Api\Samples\Php\ApiHandler;

$config = array(
  //agency values provided by DigitalResponse in the documentation
  'client' => 'CLIENT',
  'user' => 'USER',
  'password' => 'PASSWORD',
  //default values
  //'debug' => false,
  //'testing' => true,//Request will be sent to preproduction environment
  //'secure' => true,//Request will be sent through HTTPS protocol
  );

$apiHandler = new ApiHandler($config);
$source = "2";

$userEmail = "john.doe@gmail.com";
$data = array(
  "source" => $source,
  "email" => $userEmail,
  "ip" => "127.0.0.1",
  "country" => "ES",
  "language" => "ES"
  );

$res = false;
//$apiHandler->enableDebug();
if (!$apiHandler->existUser($userEmail)) {
    //$apiHandler->disableDebug();
    echo "user " . $userEmail . " doesn't exist!\n";
    $res = $apiHandler->addUser($data);
    if ($res) {
        //var_dump($res);
        echo "user " . $userEmail . " created!\n";
    } else {
        echo "error on addUser. Please debug!\n";
    }
} else {
    echo "user " . $userEmail . " exist!\n";
    $res = $apiHandler->updateUser($userEmail, $data);
    if ($res) {
        //var_dump($res);
        echo "user " . $userEmail . " updated!\n";
    } else {
        echo "error on updateUser. Please debug!\n";
    }
    
}

if ($res) {
    $user = $apiHandler->getUser($userEmail);
    if ($user) {
        //var_dump($user);
    } else {
        echo "error on getUser. Please debug!\n";
    }

    $data = array(
        "source" => $source,
        "status" => "1",
        "reason" => "other",
        "ip" => "127.0.0.1"
      );


    $subscription = $apiHandler->addSubscription($userEmail, $data);
    if ($subscription) {
        echo "user " . $userEmail . " subscribed!\n";
        //var_dump($subscription);
    } else {
        echo "error on addSubscription. Please debug!\n";
    }

    $subscriptions = $apiHandler->getSubscriptions($userEmail);
    if ($subscriptions) {
        echo "got " . $userEmail . " subscriptions!\n";
        //var_dump($subscriptions);
    } else {
        echo "error on getSubscriptions. Please debug!\n";
    }

    $data = array(
        "source" => $source,
        "name" => "promo 1",
        "description" => "Sample promo",
        "code" => "",
        "ip" => "127.0.0.1"
      );

    $promo = $apiHandler->addPromo($userEmail, $data);
    if ($promo) {
        echo "added " . $userEmail . " promo!\n";
        //var_dump($promo);
    } else {
        echo "error on addPromo. Please debug!\n";
    }

    $promos = $apiHandler->getPromos($userEmail);
    if ($promos) {
        echo "got " . $userEmail . " subscriptions!\n";
        //var_dump($promos);
    } else {
        echo "error on getPromos. Please debug!\n";
    }

    $data = array(
        "source" => $source,
        "name" => "my super product",
        "ip" => "127.0.0.1"
      );

    
    //only same favourite name per user
    $favourite = $apiHandler->addFavourite($userEmail, $data);
    if ($favourite) {
        echo "added " . $userEmail . " favourite!\n";

        //var_dump($favourite);
    } else {
        echo "error on addFavourite. Please debug!\n";
    }
  

    $favourites = $apiHandler->getFavourites($userEmail);
    if ($favourites) {
        echo "got " . $userEmail . " favourites!\n";

        //var_dump($favourites);
    } else {
        echo "error on getFavourites. Please debug!\n";
    }


    /*

    //Only one credential per user, with same username+password. Fails otherwise
    $data = array(
        "source" => $source,
        "username" => "myemail@mydomain.com",
        "password" => md5("myemail@mydomain.com"),
        "type" => 'internal',
        "ip" => "127.0.0.1"
      );

    $credential = $apiHandler->addCredential($userEmail, $data);
    if ($credential) {
        echo "added " . $userEmail . " credential!\n";

        //var_dump($credential);
    } else {
        echo "error on addCredential. Please debug!\n";
    }

    if ($credential) {
        $credentials = $apiHandler->getCredentials($userEmail);
        if ($credentials) {
            echo "got " . $userEmail . " credentials!\n";

            //var_dump($credentials);
        } else {
            echo "error on getCredentials. Please debug!\n";
        }

        $logged = $apiHandler->signin('internal', "myemail@mydomain.com", md5("myemail@mydomain.com"));
        if ($logged) {
            echo "user " . $userEmail . " logged!\n";

            //var_dump($logged);
        } else {
            echo "error on signin. Please debug!\n";
        }
    }
    */
}

