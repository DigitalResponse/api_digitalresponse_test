<?php

namespace DigitalResponse\Api\Samples\Php;

class ApiHandler
{
    private $client = "CLIENT";
    private $user = "USER";
    private $password = "PASSWORD";
    private $isTesting = true;
    private $isSecure = true;
    private $isPublic = true;
    private $debug = true;

    private $authorizationHeader = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    private $sufixUri = ".digitalresponse.es";
    

    public function __construct($config)
    {
        $this->checkParameters($config);
        
        $this->client = $config['client'];
        $this->user = $config['user'];
        $this->password = $config['password'];
        $this->isTesting = isset($config['testing']) ? $config['testing'] : $this->isTesting;
        $this->isSecure = isset($config['secure']) ? $config['secure'] : $this->isSecure;
        $this->isPublic = isset($config['public']) ? $config['public'] : $this->isPublic;
        $this->debug = isset($config['debug']) ? $config['debug'] : $this->debug;
        
        $apiUri = $this->isSecure ? "https://" : "http://";

        if ($this->isTesting) {
            $apiUri = $apiUri . "pre-";
        }

        $apiUri .= "api-" . $this->client;
        if ($this->isPublic) {
            $apiUri = $apiUri . $this->sufixUri ;
        }

        $apiUri .= "/v1";

        $this->apiUri = $apiUri;
        $this->authorizationHeader = base64_encode($this->user . ":" . md5($this->password));
    }

    public function enableDebug()
    {
        $this->debug = true;
    }

    public function disableDebug()
    {
        $this->debug = false;
    }

    private function checkParameters($config)
    {
        if (!isset($config['client']) || $config['client'] === 'CLIENT') {
            throw new \Exception("Value 'CLIENT' for param client not valid", 1);
        }

        if (!isset($config['user']) || $config['user'] === 'USER') {
            throw new \Exception("Value 'USER' for param user not valid", 1);
        }

        if (!isset($config['password']) || $config['password'] === 'PASSWORD') {
            throw new \Exception("Value 'PASSWORD' for param password not valid", 1);
        }
    }

    private function makeRequest($method, $uri, $data = null)
    {
        $curl = curl_init();

        $options = array(
            CURLOPT_URL => $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => array(
            "Authorization: Basic " . $this->authorizationHeader
            )
        );

        if (isset($data)) {
            $options[CURLOPT_POSTFIELDS] = json_encode($data);
        }
        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($this->debug) {
            echo "\nRequest:\n";
            var_dump($uri);
            var_dump($method);
            var_dump($data);

            echo "Response:\n";
            var_dump($response);
            var_dump($err);
            var_dump($info);
        }

        return array(
            'response' => $response,
            'err' => $err,
            'info' => $info
            );
    }

    private function isOkCode($status)
    {
        return $status === 200 || $status === 201;
    }

    private function isNotFound($status)
    {
        return $status === 404;
    }

    private function isBadParams($status)
    {
        return $status === 400;
    }

    private function processGetRequest($uri)
    {
        try {
            $result = $this->makeRequest('GET', $uri);
            $info = $result['info'];
            $err = $result['err'];
            $response = $result['response'];

            if ($err !== "" || !$this->isOkCode($info['http_code'])) {
                return false;
            } else {
                return json_decode($response);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function processPostRequest($uri, $data)
    {
        try {
            $result = $this->makeRequest('POST', $uri, $data);
            $info = $result['info'];
            $err = $result['err'];
            $response = $result['response'];

            if ($err !== "" || !$this->isOkCode($info['http_code'])) {
                return false;
            } else {
                return json_decode($response);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    //https://api.digitalresponse.es/docs/index.html#api-Records-PostRecords
    public function addUser($data)
    {
        return $this->processPostRequest($this->apiUri . '/records', $data);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Records-PutRecords
    public function updateUser($email, $data)
    {
        try {
            unset($data['email']);

            $uri = $this->apiUri . '/records/' . $email;
            $result = $this->makeRequest("PUT", $uri, $data);
            $info = $result['info'];
            $err = $result['err'];
            $response = $result['response'];

            if ($err !== "" || !$this->isOkCode($info['http_code'])) {
                return false;
            } else {
                return json_decode($response);
            }
        } catch (Exception $e) {
            return false;
        }
    }

    //https://api.digitalresponse.es/docs/index.html#api-Records-GetRecords
    public function getUser($email)
    {
        return $this->processGetRequest($this->apiUri . '/records/' . $email);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Records-ExistRecords
    public function existUser($email)
    {
        $response = $this->processGetRequest($this->apiUri . '/records/' . $email . '/exist');
        return isset($response->exist) && $response->exist === true;
    }

    //https://api.digitalresponse.es/docs/index.html#api-Subscriptions-PostRecordsSubscriptions
    public function addSubscription($email, $data)
    {
        return $this->processPostRequest($this->apiUri . '/records/' . $email . '/subscriptions', $data);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Subscriptions-GetRecordsSubscriptions
    public function getSubscriptions($email)
    {
        return $this->processGetRequest($this->apiUri . '/records/' . $email . '/subscriptions');
    }

    //https://api.digitalresponse.es/docs/index.html#api-Promos-PostRecordsPromos
    public function addPromo($email, $data)
    {
        return $this->processPostRequest($this->apiUri . '/records/' . $email . '/promos', $data);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Promos-GetRecordsPromos
    public function getPromos($email)
    {
        return $this->processGetRequest($this->apiUri . '/records/' . $email . '/promos');
    }

    //https://api.digitalresponse.es/docs/index.html#api-Credentials-PostRecordsCredentials
    public function addCredential($email, $data)
    {
        return $this->processPostRequest($this->apiUri . '/records/' . $email . '/credentials', $data);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Credentials-GetRecordsCredentials
    public function getCredentials($email)
    {
        return $this->processGetRequest($this->apiUri . '/records/' . $email . '/credentials');
    }

    //https://api.digitalresponse.es/docs/index.html#api-Favourites-PostRecordsFavourites
    public function addFavourite($email, $data)
    {
        return $this->processPostRequest($this->apiUri . '/records/' . $email . '/favourites', $data);
    }

    //https://api.digitalresponse.es/docs/index.html#api-Favourites-GetRecordsFavourites
    public function getFavourites($email)
    {
        return $this->processGetRequest($this->apiUri . '/records/' . $email . '/favourites');
    }

    //https://api.digitalresponse.es/docs/index.html#api-Credentials-SignInCredentials
    public function signin($type, $username, $password)
    {
        $response = $this->processGetRequest($this->apiUri . '/signin/' . $type . '/' . $username . '/' . $password);
        return isset($response->exist) && $response->exist === true;
    }
}
