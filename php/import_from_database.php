<?php

require_once __DIR__ . '/../vendor/autoload.php';// VENDOR
require_once __DIR__ . '/ApiHandler.php';

use DigitalResponse\Api\Samples\Php\ApiHandler;

$apiConfig = array(
  //agency values provided by DigitalResponse in the documentation
  'client' => 'CLIENT',
  'user' => 'USER',
  'password' => 'PASSWORD',
  'source' => 'SOURCE',
  //default values
  //'debug' => false,
  //'testing' => true,//Request will be sent to preproduction environment, adding pre-api-CLIENT.digitalresponse.es
  //'secure' => true,//Request will be sent through HTTPS protocol
  );

$database = array(
    'hostname'  => 'DB_HOST',
    'database'  => 'DB_NAME',
    'username'  => 'DB_USERNAME',
    'password'  => 'DB_PASSWORD',
    'table'     => 'TABLE'
    );

R::setup('mysql:host=' . $database['hostname'] . ';dbname=' . $database['database'], $database['username'], $database['password']); //mysql conection
R::debug(true);

$limit = 100;
    
//Replace uppercase fields with source database table fields
$sql = "SELECT 
    '' AS name, 
    '' AS lastname,
    '' AS phone,
    '' AS gender,
    '' AS birthday,
    '' AS email,
    CONCAT ('STREET_TYPE', ', ', 'STREET_ADDRESS', ', ', 'STREET_NUMBER', ', ', 'STREET_FLOOR', ' ', 'STREET_LETTER') AS fullstreetaddress,
    '' AS zipcode,
    '' AS city,
    '' AS province,
    '' AS favourite,
    '' AS username,
    '' AS twitter_id,
    '' AS facebook_id,
    '' AS password,
    '' AS privacy,
    '' AS legal,
    '' AS newsletter,
    '' AS promo_name,
    '' AS promo_desc,
    '' AS promo_code
    '' AS ip
    FROM `" . $database['table'] . "` ORDER BY `SORT_FIELD` ASC LIMIT 100";

$apiHandler = new ApiHandler($apiConfig);
$source = $apiConfig['source'];

$rows = R::getAll($sql);
foreach ($rows as $row) {
    $userEmail = $row['email'];
    $data = array(
        "source" => $source,
        "name" => $row['name'],
        "lastname" => $row['lastname'],
        "phone" => $row['phone'],
        "birthday" => $row['birthday'],
        "email" => $userEmail,
        "full_streetaddress" => $row['full_streetaddress'],
        "zipcode" => $row['zipcode'],
        "city" => $row['city'],
        "province" => $row['province'],
        "ip" => $row['ip']
    );

    $res = false;
    //$apiHandler->enableDebug();
    if (!$apiHandler->existUser($userEmail)) {
        //$apiHandler->disableDebug();
        echo "user " . $userEmail . " doesn't exist!\n";
        
        $res = $apiHandler->addUser($data);
        if ($res) {
            //var_dump($res);
            echo "user " . $userEmail . " created!\n";
        } else {
            echo "error on addUser. Please debug!\n";
        }
        
    } else {
        echo "user " . $userEmail . " exist!\n";
        
        $res = $apiHandler->updateUser($userEmail, $data);
        if ($res) {
            //var_dump($res);
            echo "user " . $userEmail . " updated!\n";
        } else {
            echo "error on updateUser. Please debug!\n";
        }
        
    }
    
    if ($res) {
        $user = $apiHandler->getUser($userEmail);
        if ($user) {
            //var_dump($user);
        } else {
            echo "error on getUser. Please debug!\n";
        }

        $data = array(
            "source" => $source,
            "status" => $row['newsletter'],
            "reason" => "other",
            "ip" => $row['ip']
          );

    
        $subscription = $apiHandler->addSubscription($userEmail, $data);
        if ($subscription) {
            echo "user " . $userEmail . " subscribed!\n";
            //var_dump($subscription);
        } else {
            echo "error on addSubscription. Please debug!\n";
        }

        $subscriptions = $apiHandler->getSubscriptions($userEmail);
        if ($subscriptions) {
            echo "got " . $userEmail . " subscriptions!\n";
            //var_dump($subscriptions);
        } else {
            echo "error on getSubscriptions. Please debug!\n";
        }

        $data = array(
            "source" => $source,
            "name" => $row['promo_name'],
            "description" => $row['promo_description'],
            "code" => $row['promo_code'],
            "privacy" => $row['privacy'],
            "legal" => $row['legal'],
            "ip" => $row['ip']
          );

        $promo = $apiHandler->addPromo($userEmail, $data);
        if ($promo) {
            echo "added " . $userEmail . " promo!\n";
            //var_dump($promo);
        } else {
            echo "error on addPromo. Please debug!\n";
        }

        $promos = $apiHandler->getPromos($userEmail);
        if ($promos) {
            echo "got " . $userEmail . " subscriptions!\n";
            //var_dump($promos);
        } else {
            echo "error on getPromos. Please debug!\n";
        }

        $data = array(
            "source" => $source,
            "name" => $row['favourite'],
            "ip" => $row['ip']
          );
        
        //only same favourite name per user
        $favourite = $apiHandler->addFavourite($userEmail, $data);
        if ($favourite) {
            echo "added " . $userEmail . " favourite!\n";

            //var_dump($favourite);
        } else {
            echo "error on addFavourite. Please debug!\n";
        }
      

        $favourites = $apiHandler->getFavourites($userEmail);
        if ($favourites) {
            echo "got " . $userEmail . " favourites!\n";

            //var_dump($favourites);
        } else {
            echo "error on getFavourites. Please debug!\n";
        }
        

        //Only one credential per user, with same username+password. Fails otherwise
        $data = array(
            "source" => $source,
            "username" => $row['username'],
            "password" => md5($row['password']),
            "type" => 'internal',
            "ip" =>  $row['ip']
          );

        $credential = $apiHandler->addCredential($userEmail, $data);
        if ($credential) {
            echo "added " . $userEmail . " internal credential!\n";

            //var_dump($credential);
        } else {
            echo "error on addCredential. Please debug!\n";
        }

                //Only one credential per user, with same username+password. Fails otherwise
        $data = array(
            "source" => $source,
            "username" => $row['facebook_id'],
            "password" => md5($row['password']),
            "type" => 'facebook',
            "ip" =>  $row['ip']
          );

        $credential = $apiHandler->addCredential($userEmail, $data);
        if ($credential) {
            echo "added " . $userEmail . " credential!\n";

            //var_dump($credential);
        } else {
            echo "error on addCredential. Please debug!\n";
        }

        $data = array(
            "source" => $source,
            "username" => $row['twitter_id'],
            "password" => md5($row['password']),
            "type" => 'twitter',
            "ip" =>  $row['ip']
          );

        $credential = $apiHandler->addCredential($userEmail, $data);
        if ($credential) {
            echo "added " . $userEmail . " credential!\n";

            //var_dump($credential);
        } else {
            echo "error on addCredential. Please debug!\n";
        }

        if ($credential) {
            $credentials = $apiHandler->getCredentials($userEmail);
            if ($credentials) {
                echo "got " . $userEmail . " credentials!\n";

                //var_dump($credentials);
            } else {
                echo "error on getCredentials. Please debug!\n";
            }

            $logged = $apiHandler->signin('internal', "myemail@mydomain.com", md5("myemail@mydomain.com"));
            if ($logged) {
                echo "user " . $userEmail . " logged!\n";

                //var_dump($logged);
            } else {
                echo "error on signin. Please debug!\n";
            }
        }
        
    }
}
