# What's this
This is a repo that contains some code scripts to integrate you system following the api specification located at
https://api.digitalresponse.es/docs/index.html

# Requeriments
To send your requests to our system, you will need to know next parameters:

1. a CLIENT value is needed to send your requests to the right container. This value has to be included in all your calls as part of the final uri to be invoked. Sample uri values depending on environment: https://api-CLIENT.digitalresponse.es (production), https://pre-api-CLIENT.digitalresponse.es (test).
2. an USER and PASSWORD values to authenticate with our system. Those values will be included as a header when requesting our system, encoded as a base64 string. Please read the documentation.
3. a SOURCE value to identify the campaign. Some campaigns have mandatory/optionals parameters, so this value is needed. This is a numeric value.

Please, contact with **DigitalResponse S.L.** to get all those values, and replace them inside the code to execute correctly those samples.

Additionally, and depending on your target language, you will need curl libraries installed in your system to execute the code samples.

# Sample code
You will find some sample php scripts in php folder.

Please, execute

``
composer install
``

to download project dependencies